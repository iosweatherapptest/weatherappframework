//
//  Weather.swift
//  WeatherFramwork
//
//  Created by sami hazel on 29/06/2023.
//

import Foundation
// MARK: - Weather
public struct Weather: Codable {
    public let id: Int?
    public let main, weatherDescription, icon: String?

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}
