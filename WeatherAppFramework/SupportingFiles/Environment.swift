//
//  Environment.swift
//  WeatherFramwork
//
//  Created by sami hazel on 29/06/2023.
//

import Foundation

public struct Environment {
    
    public enum APIError: Error {
        case decodingFailer
        case networkFailer
        case invalidURL
    }
    public enum API {
        static let baseURL: String = "https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={apiKey}&exclude=hourly,daily,alerts,minutely&units=metric&only_current={true}"
    }
}

