//
//  WeatherSDKTest.swift
//  WeatherAppFrameworkTests
//
//  Created by sami hazel on 29/06/2023.
//

import XCTest
@testable import WeatherAppFramework

class WeatherSDKTest: XCTestCase {
    
    var weatherSDK: WeatherSDK!
    static let mockApiKey: String = "c4c6b41514600215d4679681e403dac7"

    override func setUp() {
        super.setUp()
        
        // Initialize the WeatherSDK instance with a mock API key
        weatherSDK = WeatherSDK(apiKey: WeatherSDKTest.mockApiKey)
    }
    
    override func tearDown() {
        // Clean up any resources if needed
        weatherSDK = nil
        
        super.tearDown()
    }
    
    func testGetCurrentWeather_SuccessfulResponse() {
        // Given
        let lat = "37.7749"
        let lon = "-122.4194"
        let expectation = XCTestExpectation(description: "Receive current weather data")
        
        // When
        weatherSDK.getCurrentWeather(from: lat, lon: lon) { result in
            switch result {
            case .success(let response):
                // Then
                XCTAssertNotNil(response)
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Unexpected failure: \(error)")
            }
        }
        
        // Wait for the expectation to be fulfilled with a timeout
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCurrentWeather_FailureResponse() {
        // Given
        let lat = "INVALID_LATITUDE"
        let lon = "INVALID_LONGITUDE"
        let expectation = XCTestExpectation(description: "Receive error response")
        
        // When
        weatherSDK.getCurrentWeather(from: lat, lon: lon) { result in
            switch result {
            case .success:
                XCTFail("Unexpected success response")
            case .failure(let error):
                // Then
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        // Wait for the expectation to be fulfilled with a timeout
        wait(for: [expectation], timeout: 5.0)
    }
}
